

            <section id="content">
                <div class="container">
                    <div class="c-analytics row hidden-xs">

                        <div class="btn-group btn-group-justified" role="group" aria-label="...">

                            <div class="btn-group" role="group">
                                <a type="button" class="btn btn-default" href="<?= site_url('pages/new') ?>">New Page</a>
                            </div>

                            <div class="btn-group" role="group">
                                <a type="button" class="btn btn-info">Customization</a>
                            </div>

                            <div class="btn-group" role="group">
                                <a type="button" class="btn btn-warning" href="<?= site_url('') ?>" target="_blank">View Website</a>
                            </div>

                        </div>

                    </div>

                    <div class="row dash-margin">
                        <div class="col-md-3">
                            <div class="core-text">
                                <h1>Dashboard  |</h1>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="" style="display: none;">
                                <div class="alert alert-info dash-notify" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    Heads up! This alert needs your attention, but it's not super important.
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="" style="display: none;">
                        <div class="alert alert-default alert-dismissible alert-core-1" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            Well done! You successfully read this important alert message.
                        </div>
                    </div>

                    <div id="c-grid" class="clearfix" data-columns>
                        <div class="card c-dark palette-Blue-Grey bg">
                            <div class="card-header pad-btm-0">
                                <h2>Overview <small>Jump to manager on click</small></h2>
                            </div>
                            <hr>
                            <div class="row card-body card-padding core-overview">
                                <div class="col-md-6">
                                    <span>
                                        <i class="zmdi zmdi-format-color-text"></i> 
                                        <a href="<?= site_url('blogs') ?>"> 
                                            <?= $this->CoreCrud->countTableRows('blog') ?> Post
                                        </a>
                                    </span>
                                </div>
                                <div class="col-md-6">
                                    <span>
                                        <i class="zmdi zmdi-comment-list"></i> <a href=""> 1 Comment</a>
                                    </span>
                                </div>
                                <div class="col-md-6">
                                    <span>
                                        <i class="zmdi zmdi-file-plus"></i> 
                                        <a href="<?= site_url('pages') ?>"> 
                                            <?= $this->CoreCrud->countTableRows('page') ?> Page
                                        </a>
                                    </span>
                                </div>
                                <div class="col-md-6">
                                    <span>
                                        <i class="zmdi zmdi-plus-circle-o-duplicate"></i> <a href=""> 2 Content</a>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="card ">
                            <div class="card-header ch-alt">
                                <h2>Blog Draft <small>Draft a post</small></h2>
                            </div>

                            <div class="card-body card-padding">

                                <form enctype="">

                                    <div class="form-group fg-float m-b-30">
                                        <div class="fg-line">
                                            <input type="text" placeholder="Post Title" class="form-control input-sm">
                                        </div>
                                    </div>

                                    <div class="form-group fg-float">
                                        <div class="fg-line">
                                            <textarea class="form-control" placeholder="Write a quick blog post draft/memo" rows="6"></textarea>
                                        </div>
                                    </div>

                                    <div class="m-t-20">
                                        <button type="button" class="btn btn-info">Save as Draft</button>
                                    </div>

                                </form>

                                <div class="clearfix"></div>

                                <div class="pad-top-5">
                                    <p> <a href=""> <i class="zmdi zmdi-more-vert"></i> 2 Draft Post ... </a></p>
                                    <p>All Drafted post wont be published directely, you have to <a href="">open draft</a> and publish them. </p>
                                </div>

                            </div>
                        </div>

                        <div class="card c-dark palette-Blue-Only bg">
                            <div class="card-header pad-btm-0">
                                <h2>Controls <small>Theme &amp; Layout Settings</small></h2>
                            </div>
                            <hr>
                            <div class="row card-body card-padding core-overview core-overview-two">
                                <div class="col-md-4">
                                    <span>
                                        <i class="zmdi zmdi-money"></i> <a href=""> Visit Store</a>
                                    </span>
                                </div>
                                <div class="col-md-4">
                                    <span>
                                        <i class="zmdi zmdi-star-half"></i> <a href=""> Widgets</a>
                                    </span>
                                </div>
                                <div class="col-md-4">
                                    <span>
                                        <i class="zmdi zmdi-puzzle-piece"></i> <a href=""> Extensions</a>
                                    </span>
                                </div>
                                <div class="col-md-6">
                                    <span>
                                        <i class="zmdi zmdi-menu"></i> <a href=""> Menu Manager</a>
                                    </span>
                                </div>
                                <div class="col-md-6">
                                    <span>
                                        <i class="zmdi zmdi-flower"></i> <a href=""> Customize</a>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="card palette-Red-400 bg" style="display: none;">
                            <div class="pie-grid clearfix text-center">
                                <div class="col-xs-4 col-sm-6 col-md-4 pg-item">
                                    <div class="easy-pie-2 easy-pie" data-percent="92">
                                        <span class="ep-value">92</span>
                                    </div>
                                    <div class="pgi-title">Site Visit<br> Daily</div>
                                </div>
                                <div class="col-xs-4 col-sm-6 col-md-4 pg-item">
                                    <div class="easy-pie-3 easy-pie" data-percent="11">
                                        <span class="ep-value">11</span>
                                    </div>
                                    <div class="pgi-title">Email<br> Bounced</div>
                                </div>
                                <div class="col-xs-4 col-sm-6 col-md-4 pg-item">
                                    <div class="easy-pie-4 easy-pie" data-percent="52">
                                        <span class="ep-value">52</span>
                                    </div>
                                    <div class="pgi-title">Email<br> Opened</div>
                                </div>
                                <div class="col-xs-4 col-sm-6 col-md-4 pg-item">
                                    <div class="easy-pie-2 easy-pie" data-percent="44">
                                        <span class="ep-value">44</span>
                                    </div>
                                    <div class="pgi-title">Storage<br>Remaining</div>
                                </div>
                                <div class="col-xs-4 col-sm-6 col-md-4 pg-item">
                                    <div class="easy-pie-3 easy-pie" data-percent="78">
                                        <span class="ep-value">78</span>
                                    </div>
                                    <div class="pgi-title">Web Page<br> Views</div>
                                </div>
                                <div class="col-xs-4 col-sm-6 col-md-4 pg-item">
                                    <div class="easy-pie-4 easy-pie" data-percent="32">
                                        <span class="ep-value">32</span>
                                    </div>
                                    <div class="pgi-title">Server<br> Processing</div>
                                </div>
                            </div>
                        </div>

                        <div class="card c-dark palette-Amber bg" style="display: none;">
                            <div class="card-header p-b-0">
                                <h2>For the past 30 days <small>Core Statistics V1.0</small></h2>
                                <ul class="actions a-alt">
                                    <li class="dropdown">
                                        <a href="#" data-toggle="dropdown">
                                            <i class="zmdi zmdi-more-vert"></i>
                                        </a>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li>
                                                <a href="#">Change Date Range</a>
                                            </li>
                                            <li>
                                                <a href="#">Change Graph Type</a>
                                            </li>
                                            <li>
                                                <a href="#">Other Settings</a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                            <div class="card-body">
                                <div class="chart-edge">
                                    <div class="ns-chart flot-chart m-b-20" id="number-stats-chart"></div>
                                </div>

                                <div class="list-group lg-alt lg-even-white">
                                    <div class="list-group-item media">
                                        <div class="pull-right hidden-sm">
                                            <div class="sparkline-bar-1"></div>
                                        </div>

                                        <div class="media-body ns-item">
                                            <small>Page Views</small>
                                            <h3>47,896,536</h3>
                                        </div>
                                    </div>

                                    <div class="list-group-item media">
                                        <div class="pull-right hidden-sm">
                                            <div class="sparkline-bar-2"></div>
                                        </div>

                                        <div class="media-body ns-item">
                                            <small>Site Visitors</small>
                                            <h3>24,456,799</h3>
                                        </div>
                                    </div>

                                    <div class="list-group-item media">
                                        <div class="pull-right hidden-sm">
                                            <div class="sparkline-bar-3"></div>
                                        </div>

                                        <div class="media-body ns-item">
                                            <small>Total Clicks</small>
                                            <h3>13,965</h3>
                                        </div>
                                    </div>
                                </div>

                                <div class="p-5"></div>
                            </div>
                        </div>

                    </div>
                </div>
            </section>
